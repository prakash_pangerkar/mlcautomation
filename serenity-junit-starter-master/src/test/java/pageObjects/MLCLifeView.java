package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MLCLifeView {
	WebDriver driver;
	public MLCLifeView(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[@class='breadcrumbs']")
	WebElement breadcrumbs;

	@FindBy(linkText = "Request a demo")
	WebElement RequestDemo;
	
	public WebElement breadcrumb() {
		return breadcrumbs;
	}

	public String breadcrumb_gettext() {
		return breadcrumbs.getText();
	}
	
	public WebElement RequestDemo() {
		return RequestDemo;
	}

	public void Click_RequestDemo() {
		RequestDemo.click();;
	}

}
