package generic;

public class Parameters {

//****************Parameters for MLC Life Testing*******************
	public static final String strname = "Paul Smyth";
	public static final String strCompany = "National Australia Bank";
	public static final String strEmail = "abc.xyz@email.com";
	public static final String strPhone = "0456123789";
	public static final String strPref_Date = "25-Mar-2020";
	public static final String strReq_Details = "Please automate the script asap";
	public static final String strExpectedtext = "Home Partnering with us Superannuation funds LifeView";
	
	
//****************Parameters for Australia Post API Testing*******************	
	public static final String urlPrefix = "digitalapi.auspost.com.au";
	public static final String postageTypesURL = "https://" + urlPrefix + "/postage/parcel/international/calculate.json?";
	public static final String apiKey = "eaf3799d-1a79-4973-a8f4-2df13f30e7e0";
	
	public static final String strcountrycode1 = "AL";
	public static final String strweight1 = "1.5";
	public static final String strservicecode1 = "INT_PARCEL_AIR_OWN_PACKAGING";
	public static final String TotalCost1 = "\"total_cost\":\"61.30\"";

	public static final String strcountrycode2 = "NZ";
	public static final String strweight2 = "2";
	public static final String strservicecode2 = "INT_PARCEL_EXP_OWN_PACKAGING";
	public static final String TotalCost2 = "\"total_cost\":\"50.20\"";

	public static final String strcountrycode3 = "IN";
	public static final String strweight3 = "3";
	public static final String strservicecode3 = "INT_PARCEL_STD_OWN_PACKAGING";
	public static final String TotalCost3 = "\"total_cost\":\"65.95\"";

	
	
//****************Parameters for ATO Calculator Testing*******************	
	public static final String strincyear1 = "2018-19";
	public static final String strincome1 = "90000";
	public static final String strresidencystatus1 = "Resident for full year";
	public static final String strtax1 = "$20,797.00";
	
	public static final String strincyear2 = "2016-17";
	public static final String strincome2 = "80000";
	public static final String strresidencystatus2 = "Non-resident for full year";
	public static final String strtax2 = "$26,000.00";

	public static final String strincyear3 = "2014-15";
	public static final String strincome3 = "70000";
	public static final String strresidencystatus3 = "Part-year resident";
	public static final String strresmonths3 = "3";
	public static final String strtax3 = "$14,971.88";
	
}
