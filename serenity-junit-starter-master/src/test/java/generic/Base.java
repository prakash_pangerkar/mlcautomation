package generic;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

public class Base extends Parameters{
	WebDriver driver;
	Properties projConfig = null;
	FileInputStream fs_projConfig = null;
	FileInputStream fs_objRep = null;

	public void SetUp() throws IOException{
		
	//Initializing the Properties object
		projConfig = new Properties();
		fs_projConfig = new FileInputStream(System.getProperty("user.dir")+"\\src\\test\\java\\Config.properties");
		projConfig.load(fs_projConfig);
		
	
	}
	
	
	//Open the web application
	//Input	  			:	driver & strURL
	//Output   			: 	Opens the MLC/ATO web application
	//Last Modified By  :	Prakash
	//Last Modified On  :	March 21, 2020	
	
	public WebDriver LaunchApplication(WebDriver driver, String strurl) {
		driver.get(projConfig.getProperty(strurl));		
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		return driver;
	}
	

}
