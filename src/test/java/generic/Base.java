package generic;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

public class Base extends Parameters{
	WebDriver driver;
	Properties projConfig = null;
	FileInputStream fs_projConfig = null;
	FileInputStream fs_objRep = null;

	public void SetUp() throws IOException{
		
	//Initializing the Properties object
		projConfig = new Properties();
		fs_projConfig = new FileInputStream(System.getProperty("user.dir")+"\\src\\test\\java\\user.properties");
		projConfig.load(fs_projConfig);
		
	
	}
	
	
	//Open the Telstra Health web application
	//Input	  			:	void
	//Output   			: 	Opens the Telstra Health web application
	//Last Modified By  :	Prakash
	//Last Modified On  :	Feb 03, 2017	
	
	public WebDriver LaunchApplication(WebDriver driver, String strurl) {
		// TODO Auto-generated method stub
		driver.get(projConfig.getProperty(strurl));		
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		return driver;
	}
	

}
