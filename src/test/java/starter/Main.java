package starter;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.testng.asserts.Assertion;

import pageObjects.ATO_Calculcator;
import pageObjects.PostageCalulcator;
import generic.Base;
import pageObjects.MLCHome;
import pageObjects.MLCLifeView;
import pageObjects.MLCLifeViewDemo;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;

@RunWith(SerenityRunner.class)

public class Main extends Base{
	@Managed
	WebDriver driver;

	@Test
	public void mlclifeviewdemo() throws InterruptedException, IOException 
	{
		SetUp();
		String strURL = "ATO";
		driver = LaunchApplication(driver, strURL);

		//Create Page Objects
		MLCHome homepage = new MLCHome(driver);
		MLCLifeView lifepage = new MLCLifeView(driver);
		MLCLifeViewDemo lifeviewdemo = new MLCLifeViewDemo(driver);

		//MLCHome Page
		homepage.click_Partnering(); // MouseClick event opens the options instead of MouseHover
		homepage.click_LifeView();

		//MLC Life View Page
		String Expectedbctext = "Home Partnering with us Superannuation funds LifeView";
		String Actualbctext = (lifepage.breadcrumb_gettext());
		
		Assertion breadcrumb = new Assertion();
		breadcrumb.assertEquals(Actualbctext, Expectedbctext);
		
		lifepage.Click_RequestDemo();

		//MLC LifeView Demo Page
		lifeviewdemo.EnterUserDetails();
		Thread.sleep(10000);
	}
	
	
	
	@Test
	public void TaxCalculator1() throws InterruptedException, IOException 
	{
		SetUp();
		String strURL = "ATO";
		driver = LaunchApplication(driver, strURL);

		ATO_Calculcator calc_page1 = new ATO_Calculcator(driver);
		calc_page1.enter_taxdetails(strincyear1, strincome1, strresidencystatus1);
		calc_page1.verify_tax(strtax1);
	}
	
	@Test
	public void TaxCalculator2() throws InterruptedException, IOException 
	{
		SetUp();
		String strURL = "ATO";
		driver = LaunchApplication(driver, strURL);

		ATO_Calculcator calc_page2 = new ATO_Calculcator(driver);
		calc_page2.enter_taxdetails(strincyear2, strincome2, strresidencystatus2);
		calc_page2.verify_tax(strtax2);
	}

	@Test
	public void TaxCalculator3() throws InterruptedException, IOException 
	{
		SetUp();
		String strURL = "ATO";
		driver = LaunchApplication(driver, strURL);

		ATO_Calculcator calc_page3 = new ATO_Calculcator(driver);	
		calc_page3.enter_taxdetails(strincyear3, strincome3, strresidencystatus3,strresmonths3);
		calc_page3.verify_tax(strtax3);
	}

	
	@Test
	public void calculatepostage() throws Exception {
		PostageCalulcator cis = new PostageCalulcator();
		cis.htmlapitest(strcountrycode1, strweight1, strservicecode1, TotalCost1);
		cis.htmlapitest(strcountrycode2, strweight2, strservicecode2, TotalCost2);
		cis.htmlapitest(strcountrycode3, strweight3, strservicecode3, TotalCost3);
	}

	
}
