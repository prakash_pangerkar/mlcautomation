package pageObjects;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public class PostageCalulcator extends Parameters{

	String apiKey = "eaf3799d-1a79-4973-a8f4-2df13f30e7e0";
	// Set the URL for the International Parcel Calculation service
	String urlPrefix = "digitalapi.auspost.com.au";
	String postageTypesURL = "https://" + urlPrefix + "/postage/parcel/international/calculate.json?";

	
	public void htmlapitest(String strcountrycode, String strweight, String strservicecode, String strTotalCost) throws Exception {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("country_code", strcountrycode));
		params.add(new BasicNameValuePair("weight", strweight));
		params.add(new BasicNameValuePair("service_code", strservicecode));
		String query = URLEncodedUtils.format(params, "UTF-8");
		
			HttpUriRequest httpGet = new HttpGet(postageTypesURL + query);
			httpGet.setHeader("AUTH-KEY", apiKey);
			HttpResponse response = HttpClientBuilder.create().build().execute(httpGet);
			// Check the response: if the body is empty then an error occurred
			if(response.getStatusLine().getStatusCode() != 200){
			  throw new Exception("Error: '" + response.getStatusLine().getReasonPhrase() + "' - Code: " + response.getStatusLine().getStatusCode());
			}
			
			   String jsonResponse = EntityUtils.toString(response.getEntity());	   
			   JSONParser parser = new JSONParser();
			   JSONObject jsonObject = (JSONObject) parser.parse(jsonResponse);
			   String strjsonresponse = jsonObject.toJSONString();
			   System.out.println("********************************************************************************************************************************");
			   System.out.println("********************************************************************************************************************************");
			   System.out.println("Total_cost for the postage = " + jsonObject.get("postage_result"));
			   System.out.println("********************************************************************************************************************************");
			   System.out.println("********************************************************************************************************************************");
			   assertTrue(strjsonresponse.contains(strTotalCost));
	
		}
	}

