package pageObjects;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MLCLifeViewDemo {
	WebDriver driver;
	public MLCLifeViewDemo(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	@FindBy(id = "wffm0635571898ed434b8db4317b0d7a8d19_Sections_0__Fields_0__Value")
	WebElement Name;
	
	@FindBy(id = "wffm0635571898ed434b8db4317b0d7a8d19_Sections_0__Fields_1__Value")
	WebElement Company;
	
	@FindBy(id = "wffm0635571898ed434b8db4317b0d7a8d19_Sections_0__Fields_2__Value")
	WebElement Email;

	@FindBy(id = "wffm0635571898ed434b8db4317b0d7a8d19_Sections_0__Fields_3__Value")
	WebElement Phone;
	
	@FindBy(id = "wffm0635571898ed434b8db4317b0d7a8d19_Sections_0__Fields_4__Value")
	WebElement PDate;
	
	@FindBy(xpath = "//input[@value='PM']")
	WebElement PTimePM;
	
	@FindBy(xpath = "//input[@value='AM']")
	WebElement PTimeAM;

	@FindBy(id = "wffm0635571898ed434b8db4317b0d7a8d19_Sections_0__Fields_6__Value")
	WebElement Req_Details;
	
	public void EnterUserDetails() 
	{
		try {
			FileInputStream Locator = new FileInputStream((System.getProperty("user.dir")+"\\src\\test\\java\\user.properties"));
			
			Properties prop = new Properties();
			prop.load(Locator);
			String strname = prop.getProperty("name");
			String strCompany = prop.getProperty("Company");
			String strEmail = prop.getProperty("Email");
			String strPhone = prop.getProperty("Phone");
			String strPref_Date = prop.getProperty("Pref_Date");
			String strReq_Details = prop.getProperty("Req_Details");
	
			Name.sendKeys(strname);
			Company.sendKeys(strCompany);
			Email.sendKeys(strEmail);
			Phone.sendKeys(strPhone);
			PDate.clear();
			PDate.sendKeys(strPref_Date);
			PTimePM.click();
			Req_Details.sendKeys(strReq_Details);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	
	
	}
	

}
