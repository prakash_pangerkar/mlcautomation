package pageObjects;

public class ATO_values {

	public static final String strURL = "https://www.ato.gov.au/Calculators-and-tools/Host/?anchor=STC&anchor=STC#STC/questions";
	
	public static final String strincyear1 = "2018-19";
	public static final String strincome1 = "90000";
	public static final String strresidencystatus1 = "Resident for full year";
	public static final String strtax1 = "$20,797.00";
	
	public static final String strincyear2 = "2016-17";
	public static final String strincome2 = "80000";
	public static final String strresidencystatus2 = "Non-resident for full year";
	public static final String strtax2 = "$26,000.00";

	public static final String strincyear3 = "2014-15";
	public static final String strincome3 = "70000";
	public static final String strresidencystatus3 = "Part-year resident";
	public static final String strresmonths3 = "3";
	public static final String strtax3 = "$14,971.88";
	
}
