package auspostapi_testcases;

public class Parameters {

//****************Parameters for Australia Post API*******************	
	public static final String urlPrefix = "digitalapi.auspost.com.au";
	public static final String postageTypesURL = "https://" + urlPrefix + "/postage/parcel/international/calculate.json?";
	
	public static final String strcountrycode1 = "AL";
	public static final String strweight1 = "1.5";
	public static final String strservicecode1 = "INT_PARCEL_AIR_OWN_PACKAGING";
	public static final String TotalCost1 = "\"total_cost\":\"61.30\"";

	public static final String strcountrycode2 = "NZ";
	public static final String strweight2 = "2";
	public static final String strservicecode2 = "INT_PARCEL_EXP_OWN_PACKAGING";
	public static final String TotalCost2 = "\"total_cost\":\"50.20\"";

	public static final String strcountrycode3 = "IN";
	public static final String strweight3 = "3";
	public static final String strservicecode3 = "INT_PARCEL_STD_OWN_PACKAGING";
	public static final String TotalCost3 = "\"total_cost\":\"65.95\"";

}
