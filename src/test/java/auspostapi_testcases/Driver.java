package auspostapi_testcases;
import org.junit.Test;
import org.junit.runner.RunWith;

import net.serenitybdd.junit.runners.SerenityRunner;

@RunWith(SerenityRunner.class)

public class Driver extends Parameters{

		@Test
		public void calculatepostage() throws Exception {
			PostageCalulcator cis = new PostageCalulcator();
			cis.htmlapitest(strcountrycode1, strweight1, strservicecode1, TotalCost1);
			cis.htmlapitest(strcountrycode2, strweight2, strservicecode2, TotalCost2);
			cis.htmlapitest(strcountrycode3, strweight3, strservicecode3, TotalCost3);
		}

	}
