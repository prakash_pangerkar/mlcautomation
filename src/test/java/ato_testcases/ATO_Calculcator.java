package ato_testcases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class ATO_Calculcator {
	WebDriver driver;
	public ATO_Calculcator(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//select[@name='ddl-financialYear']")
	WebElement income_year;

	@FindBy(name = "texttaxIncomeAmt")
	WebElement taxable_income;
	
	@FindBy(xpath = "//input[@value='resident']/../label/span[2]")
	WebElement Resident_FY;
	
	@FindBy(xpath = "//input[@value='nonResident']/../label/span[2]")
	WebElement NonResident_FY;
	
	@FindBy(xpath = "//input[@value='partYearResident']/../label/span[2]")
	WebElement Resident_PY;

	@FindBy(xpath = "//select[@name='ddl-residentPartYearMonths']")
	WebElement resident_months;

	@FindBy(id = "bnav-n1-btn4")
	WebElement submit;

	@FindBy(xpath = "//*[text()='The estimated tax on your taxable income is ']/span")
	WebElement taxamount;
	
	
	public void selectincomeyear(String strincyear) {
		Select siy = new Select(income_year);
		siy.selectByVisibleText(strincyear);
	}
	
	public void entertaxableincome(String strincome) {
		taxable_income.clear();
		taxable_income.sendKeys(strincome);
	}
	
	public void selectresidencyStatus(String strresidencystatus) {
		switch(strresidencystatus) {
			case "Resident for full year" : Resident_FY.click();
				break;
			case "Non-resident for full year": NonResident_FY.click();
				break;
			case "Part-year resident": Resident_PY.click();
			
				break;
		}			
	}
	
	public void selectresidentmonths(String strresmonths) {
		Select srm = new Select(resident_months);
		srm.selectByVisibleText(strresmonths);		
	}
	
	public void click_submit() {
		submit.click();
	}

	public void enter_taxdetails(String year, String income, String rstatus) throws InterruptedException {
		selectincomeyear(year);
		entertaxableincome(income);
		selectresidencyStatus(rstatus);		
		click_submit();
		Thread.sleep(3000);
	}
	
	public void enter_taxdetails(String year, String income, String rstatus, String rmonths) throws InterruptedException {
		selectincomeyear(year);
		entertaxableincome(income);
		selectresidencyStatus(rstatus);
		selectresidentmonths(rmonths);
		click_submit();
		Thread.sleep(3000);
	}

	public void verify_tax(String strexpectedtax) {
		String stractualtax = taxamount.getText();
		System.out.println("The estimated tax amount calculated is " + stractualtax);
		Assert.assertEquals(stractualtax, strexpectedtax);
	}
}
