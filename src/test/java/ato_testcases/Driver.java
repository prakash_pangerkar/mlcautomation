package ato_testcases;

import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;

@RunWith(SerenityRunner.class)
public class Driver extends ATO_values{

	@Managed
	WebDriver driver;
	
	@Test
	public void TaxCalculator1() throws InterruptedException 
	{
		driver = LaunchApplication(driver, strURL);

		ATO_Calculcator calc_page1 = new ATO_Calculcator(driver);
		calc_page1.enter_taxdetails(strincyear1, strincome1, strresidencystatus1);
		calc_page1.verify_tax(strtax1);
	}

	@Test
	public void TaxCalculator2() throws InterruptedException 
	{
		driver = LaunchApplication(driver, strURL);

		ATO_Calculcator calc_page2 = new ATO_Calculcator(driver);
		calc_page2.enter_taxdetails(strincyear2, strincome2, strresidencystatus2);
		calc_page2.verify_tax(strtax2);
	}

	@Test
	public void TaxCalculator3() throws InterruptedException 
	{
		driver = LaunchApplication(driver, strURL);

		ATO_Calculcator calc_page3 = new ATO_Calculcator(driver);	
		calc_page3.enter_taxdetails(strincyear3, strincome3, strresidencystatus3,strresmonths3);
		calc_page3.verify_tax(strtax3);
	}
	
	private WebDriver LaunchApplication(WebDriver driver, String strurl) {
		// TODO Auto-generated method stub
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		driver.get(strurl);
		return driver;
	}

}
