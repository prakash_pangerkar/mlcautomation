package mlc_testcases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MLCHome {
	
	WebDriver driver;
	public MLCHome(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = ".//a[@href='/partnering-with-us']")
	WebElement Partnering;
	
	@FindBy(linkText="LifeView")
	WebElement LifeView;
	

	public void click_LifeView(){
		LifeView.click();;
	}

	public void click_Partnering() {
		Partnering.click();
	}

}
