package mlc_testcases;

import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.testng.asserts.Assertion;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;

@RunWith(SerenityRunner.class)
public class Driver {

	@Managed
	WebDriver driver;
	
	@Test
	public void mlclifeviewdemo() throws InterruptedException 
	{
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		driver.get("https://www.mlcinsurance.com.au/");

		//Create Page Objects
		MLCHome homepage = new MLCHome(driver);
		MLCLifeView lifepage = new MLCLifeView(driver);
		MLCLifeViewDemo lifeviewdemo = new MLCLifeViewDemo(driver);

		//MLCHome Page
		homepage.click_Partnering(); // MouseClick event opens the options instead of MouseHover
		homepage.click_LifeView();

		//MLC Life View Page
		String Expectedbctext = "Home Partnering with us Superannuation funds LifeView";
		String Actualbctext = (lifepage.breadcrumb_gettext());
		
		Assertion breadcrumb = new Assertion();
		breadcrumb.assertEquals(Actualbctext, Expectedbctext);
		
		lifepage.Click_RequestDemo();

		//MLC LifeView Demo Page
		lifeviewdemo.EnterUserDetails();
		Thread.sleep(10000);
	}
}
